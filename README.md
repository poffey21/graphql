# GraphQL

Snippets of GraphQL

To use, head to [GraphQL Explorer](https://gitlab.com/-/graphql-explorer). 

See [the GraphQL docs](https://docs.gitlab.com/ee/api/graphql/) for more info.


## This will give you all tags for a specific group:

```
{
  group(fullPath: "gitlab-com") {
    projects(includeSubgroups: true, first: 10000) {
      nodes {
        name
        fullPath
        tagList
      }
    }
  }
}
```


## This will give you time spent by user

```
{
  group(fullPath: "gitlab-org") {
    timelogs(startDate:"2019-10-19", endDate: "2019-12-18"){
      edges {
        node {
          date
          timeSpent
          user {
            username
          }
          issue {
            title
            labels {
                nodes {
                  title
                }
            }
            epic {
              title
              id
            }
            milestone {
              title
            }
          }
        }
      }
    }
  }
}
```
